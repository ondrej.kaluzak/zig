inlets = 1;
outlets = 1;


var COUNT = 24;
var counter = 0;
var MIN = 0;
var MAX = 24;

function reset(){
	counter = 0;
	outlet(0, counter);
}

function bang() {
  	var result = 0;
	counter = (counter + 1)% (COUNT*2);
	result = (counter)%COUNT;
	result += MIN;
  	outlet(0, result);
}

function max(){
	var a = arrayfromargs(messagename, arguments);
	MAX = a[1];
	COUNT = MAX - MIN;
}

function min(){
	var a = arrayfromargs(messagename, arguments);
	MIN = a[1];
	COUNT = MAX - MIN;
}

function msg_int(){
	var a = arrayfromargs(messagename, arguments);
	counter = (a[0])% (COUNT*2);
	outlet(0, a[0]);
}


function info(){

	post('\n  ************ INFO ************ \n');
	post('MIN = ', MIN);
	post('\n');
	post('MAX = ', MAX);
	post('\n');
	post('COUNTER = ', counter);
	post('\n');
	post('COUNT = ', COUNT);
	post('\n');

}
